defmodule Server.Alfheim do
  @moduledoc """
  Модуль общения с сервисом вторичной обработки данных
  """
  use Server.CallerFuncs

  use Munin,
    adapter: Munin.FileWritter,
    otp_app: :server,
    log_path: "priv/unhandled_logs",
    service_name: "alfheim"

  def important_list, do: ~w()a

  def get_handled_packs(auth, data) do
    # node = :"alfheim@172.25.78.151"

    # if node in Node.list() do
    call({:handle_test, auth, data})
    # else
    # Node.connect(node)
    # |> try_connect_node_and_send_packs(auth, data)
    # end
  end

  def get_handled_packs(auth, data, for_merged) do
    # node = :"alfheim@172.25.78.151"

    # if node in Node.list() do
    call({:handle_test, auth, data, for_merged})
    # else
    # Node.connect(node)
    # |> try_connect_node_and_send_packs(auth, data)
    # end
  end

  def try_connect_node_and_send_packs(true, auth, data) do
    :timer.sleep(3000)
    call({:handle_test, auth, data})
  end
end
