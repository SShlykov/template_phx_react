defmodule Server.CallerFuncs do
  @moduledoc """
  Макрос, чтобы не таскать одинаковые и при этом необходимые модулям Munin функции
  """
  defmacro __using__([]) do
    alias RFiler, as: Filer

    quote do
      def read_file(filename), do: Filer.read_file(filename)
      def count_log(filename), do: Filer.count_logs(filename)
      def read_file_and_remove(filename), do: Filer.read_file_and_remove(filename)
      def write_file(data, filename, ext), do: Filer.write_file(data, filename, ext)
    end
  end
end
