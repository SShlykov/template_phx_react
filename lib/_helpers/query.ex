defmodule Helpers.SqlSupQuery do
  alias Server.Repo
  import Ecto.Query, warn: false

  def run_query(query, db_atom) when is_binary(query) do
    db_params = Server.GStore.get_db("GStore", db_atom)
    Server.SqlSup.run_query(db_params, query)
  end

  def run_query(query, db_atom) do
    db_params = Server.GStore.get_db("GStore", db_atom)
    query = crate_raw_query(query) |> String.replace("\"", "")

    resultify(Server.SqlSup.run_query(db_params, query))
  end

  def crate_raw_query(%Ecto.Query{} = ecto_query),
    do: crate_raw_query(Repo.to_sql(:all, ecto_query))

  def crate_raw_query({string, params}) do
    len = length(params)

    1..len
    |> Enum.uniq()
    |> Enum.reduce(string, fn num, acc ->
      String.replace(acc, "$#{num}", querify(Enum.at(params, num - 1)))
    end)
  end

  def querify(param) when is_list(param), do: ~s|ARRAY[#{Enum.join(param, ", ")}]|
  def querify(param) when is_binary(param), do: "'#{param}'"
  def querify(%NaiveDateTime{} = param), do: "'#{param}'"
  def querify(%Date{} = param), do: "'#{param}'"
  def querify(param), do: "#{param}"

  def resultify([res]), do: res
  def resultify(res), do: res
end
