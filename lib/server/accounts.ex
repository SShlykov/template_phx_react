defmodule Server.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias Server.Niflheim
  @site_name Application.get_env(:server, :site_name)

  def get_user_permissions(id), do: Niflheim.get_user_permissions(@site_name, id)

  ## Database getters

  @doc """
  Gets a user by login.

  ## Examples

      iex> get_user_by_login("foo@example.com")
      %User{}

      iex> get_user_by_login("unknown@example.com")
      nil

  """
  def get_user_by_login(login) when is_binary(login),
    do: Niflheim.get_user_by_login(@site_name, login)

  @doc """
  Gets a user role by id.

  ## Examples

      iex> get_user_role(1)
      10

      iex> get_user_role(-1)
      nil

  """
  def get_user_role(user_id), do: Niflheim.get_user_role(@site_name, user_id)

  @doc """
  Gets a user by login and password.

  ## Examples

      iex> get_user_by_login_and_password("foo@example.com", "correct_password")
      %User{}

      iex> get_user_by_login_and_password("foo@example.com", "invalid_password")
      nil

  """
  def get_user_by_login_and_password(login, password)
      when is_binary(login) and is_binary(password),
      do: Niflheim.get_user_by_login_and_password(@site_name, login, password)

  @doc """
  Gets a single user.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Niflheim.get_user(@site_name, id)

  ## User registration

  @doc """
  Registers a user.

  ## Examples

      iex> register_user(%{field: value})
      {:ok, %User{}}

      iex> register_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def register_user(attrs), do: Niflheim.register_user(@site_name, attrs)

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user_registration(user)
      %Ecto.Changeset{data: %User{}}

  """
  def change_user_registration(user, attrs \\ %{}),
    do: Niflheim.change_user_registration(user, attrs)

  ## Settings

  @doc """
  Returns an `%Ecto.Changeset{}` for changing the user login.

  ## Examples

      iex> change_user_login(user)
      %Ecto.Changeset{data: %User{}}

  """
  def change_user_login(user, attrs \\ %{}), do: Niflheim.change_user_login(user, attrs)

  @doc """
  Emulates that the login will change without actually changing
  it in the database.

  ## Examples

      iex> apply_user_login(user, "valid password", %{login: ...})
      {:ok, %User{}}

      iex> apply_user_login(user, "invalid password", %{login: ...})
      {:error, %Ecto.Changeset{}}

  """
  def apply_user_login(user, password, attrs),
    do: Niflheim.apply_user_login(user, password, attrs)

  @doc """
  Updates the user login using the given token.

  If the token matches, the user login is updated and the token is deleted.
  The confirmed_at date is also updated to the current time.
  """
  def update_user_login(user, token), do: Niflheim.update_user_login(user, token)

  @doc """
  Delivers the update login instructions to the given user.

  ## Examples

      iex> deliver_update_email_instructions(user, current_login, &Routes.user_update_login_url(conn, :edit, &1))
      {:ok, %{to: ..., body: ...}}

  """
  def deliver_update_login_instructions(user, current_login, update_login_url_fun)
      when is_function(update_login_url_fun, 1),
      do: Niflheim.deliver_update_login_instructions(user, current_login, update_login_url_fun)

  @doc """
  Returns an `%Ecto.Changeset{}` for changing the user password.

  ## Examples

      iex> change_user_password(user)
      %Ecto.Changeset{data: %User{}}

  """
  def change_user_password(user, attrs \\ %{}), do: Niflheim.change_user_password(user, attrs)

  @doc """
  Updates the user password.

  ## Examples

      iex> update_user_password(user, "valid password", %{password: ...})
      {:ok, %User{}}

      iex> update_user_password(user, "invalid password", %{password: ...})
      {:error, %Ecto.Changeset{}}

  """
  def update_user_password(user, password, attrs),
    do: Niflheim.update_user_password(user, password, attrs)

  ## Session

  @doc """
  Generates a session token.
  """
  def generate_user_session_token(user), do: Niflheim.generate_user_session_token(user)

  @doc """
  Gets the user with the given signed token.
  """
  def get_user_by_session_token(token), do: Niflheim.get_user_by_session_token(token)

  @doc """
  Deletes the signed token with the given context.
  """
  def delete_session_token(token), do: Niflheim.delete_session_token(token)

  ## Confirmation

  @doc """
  Delivers the confirmation login instructions to the given user.

  ## Examples

      iex> deliver_user_confirmation_instructions(user, &Routes.user_confirmation_url(conn, :edit, &1))
      {:ok, %{to: ..., body: ...}}

      iex> deliver_user_confirmation_instructions(confirmed_user, &Routes.user_confirmation_url(conn, :edit, &1))
      {:error, :already_confirmed}

  """
  def deliver_user_confirmation_instructions(user, confirmation_url_fun)
      when is_function(confirmation_url_fun, 1),
      do: Niflheim.deliver_user_confirmation_instructions(user, confirmation_url_fun)

  @doc """
  Confirms a user by the given token.

  If the token matches, the user account is marked as confirmed
  and the token is deleted.
  """
  def confirm_user(token), do: Niflheim.confirm_user(token)

  ## Reset password

  @doc """
  Delivers the reset password login to the given user.

  ## Examples

      iex> deliver_user_reset_password_instructions(user, &Routes.user_reset_password_url(conn, :edit, &1))
      {:ok, %{to: ..., body: ...}}

  """
  def deliver_user_reset_password_instructions(user, reset_password_url_fun)
      when is_function(reset_password_url_fun, 1),
      do: Niflheim.deliver_user_reset_password_instructions(user, reset_password_url_fun)

  @doc """
  Gets the user by reset password token.

  ## Examples

      iex> get_user_by_reset_password_token("validtoken")
      %User{}

      iex> get_user_by_reset_password_token("invalidtoken")
      nil

  """
  def get_user_by_reset_password_token(token),
    do: Niflheim.get_user_by_reset_password_token(token)

  @doc """
  Resets the user password.

  ## Examples

      iex> reset_user_password(user, %{password: "new long password", password_confirmation: "new long password"})
      {:ok, %User{}}

      iex> reset_user_password(user, %{password: "valid", password_confirmation: "not the same"})
      {:error, %Ecto.Changeset{}}

  """
  def reset_user_password(user, attrs), do: Niflheim.reset_user_password(user, attrs)
end
