defmodule LiveTrack.TcpClient do
  @moduledoc false

  def connect("gis.ru.net", port), do: connect("172.25.78.153", port)

  def connect(address, port) do
    tcp_attrs = [:binary, active: false, reuseaddr: true]

    {:ok, _socket} = :gen_tcp.connect(String.to_charlist(address), port, tcp_attrs)
  rescue
    _e ->
      {:error, "can`t connect"}
  end
end
