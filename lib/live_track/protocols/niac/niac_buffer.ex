defmodule LiveTrack.Structures.NiacBuffer do
  alias Niac.Packages.{Login, DataPackage}
  alias LiveTrack.Packages.Helpers

  defstruct version: nil,
            type: __MODULE__,
            messages: [],
            opts: nil,
            auth: nil,
            log: nil,
            respond: nil,
            finish: false

  def pack_types do
    [
      L: %{
        type: "Login",
        structure: Login
      },
      P: %{
        type: "Data Pack",
        structure: DataPackage
      }
    ]
  end

  def new(line, opts \\ []) do
    opts = Keyword.merge([log: true, respond: true, validate: false], opts)
    {type, parsed_message} = read_pack(line)

    protocol_version = hd(parsed_message)

    case read_message(parsed_message, to_number(protocol_version), type) do
      {:ok, parsed_message} ->
        res =
          %__MODULE__{
            version: protocol_version,
            auth: parsed_message,
            messages: [parsed_message],
            opts: opts
          }
          |> Helpers.put_log(parsed_message, protocol_version)
          |> Helpers.put_respond(parsed_message)

        {:ok, res}

      msg ->
        {:error, msg}
    end
  rescue
    _E ->
      {:error, "ошибка при создании буфера"}
  end

  def add(%__MODULE__{version: protocol_version, messages: messages} = mod, line) do
    {type, parsed_message} = read_pack(line)

    case read_message(parsed_message, to_number(protocol_version), type) do
      {:ok, %{is_end: true, buffer: buffer}} ->
        %__MODULE__{buffer | finish: true}

      {:ok, parsed_message} ->
        %__MODULE__{mod | messages: [parsed_message | messages]}
        |> Helpers.put_log(parsed_message, protocol_version)
        |> Helpers.put_respond(parsed_message)

      _msg ->
        %__MODULE__{mod | finish: true}
    end
  end

  def read_pack(line) do
    parsed_message =
      line |> String.replace("\r", "") |> String.replace("\n", "") |> String.split(",")

    type =
      case hd(parsed_message) do
        "2" -> "L"
        "0" -> "F"
        _ -> "P"
      end

    {type, parsed_message}
  end

  def read_message(msg, _protocol_version, _type) do
    {:ok, msg}
  end

  def is_valid(line) do
    {type, _} = read_pack(line)

    is_valid_type(type)
  rescue
    _e -> false
  end

  def is_valid_login(line) do
    {type, _} = read_pack(line)

    type == "L"
  rescue
    _e -> false
  end

  defp is_valid_type(type), do: type in Keyword.keys(pack_types())

  defp to_number(num_str), do: to_float(num_str) || to_int(num_str) || :error

  defp to_float(num_str) do
    String.to_float(num_str)
  rescue
    _e -> nil
  end

  defp to_int(num_str) do
    String.to_integer(num_str)
  rescue
    _e -> nil
  end
end
