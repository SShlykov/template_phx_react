defmodule Wialon.Packages do
  def put_respond(mod, respond) do
    {resp, _} = Code.eval_string(respond, Map.to_list(mod))
    Map.put(mod, :respond, resp)
  rescue
    _e ->
      Map.put(mod, :respond, "#AL#0\r\n")
  end
end

defimpl Inspect, for: Wialon.Packages.Login do
  import Inspect.Algebra

  def inspect(%Wialon.Packages.Login{} = package, opts) do
    list =
      for attr <- [:status, :body] do
        {attr, Map.get(package, attr)}
      end

    container_doc("#Login<", list, ">", opts, fn
      {:status, status}, opts -> concat("status: ", to_doc(status, opts))
      {:body, body}, opts -> concat("body: ", to_doc(body, opts))
    end)
  end
end

defimpl Inspect, for: Wialon.Packages.DataPackage do
  import Inspect.Algebra

  def inspect(%Wialon.Packages.DataPackage{ptype: ptype} = package, opts) do
    list =
      for attr <- [:status, :body] do
        {attr, Map.get(package, attr)}
      end

    container_doc("##{ptype}<", list, ">", opts, fn
      {:status, status}, opts -> concat("status: ", to_doc(status, opts))
      {:body, body}, opts -> concat("body: ", to_doc(body, opts))
    end)
  end
end

defimpl Inspect, for: Wialon.Packages.BlackBox do
  import Inspect.Algebra

  def inspect(%Wialon.Packages.BlackBox{} = package, opts) do
    list =
      for attr <- [:status, :body] do
        {attr, Map.get(package, attr)}
      end

    container_doc("#BlackBox<", list, ">", opts, fn
      {:status, status}, opts -> concat("status: ", to_doc(status, opts))
      {:body, body}, opts -> concat("body: ", to_doc(body, opts))
    end)
  end
end

defimpl Inspect, for: Wialon.Packages.Ping do
  import Inspect.Algebra

  def inspect(%Wialon.Packages.Ping{}, opts) do
    container_doc("#Pong<", [], ">", opts, fn
      {:status, status}, opts -> concat("status: ", to_doc(status, opts))
      {:body, body}, opts -> concat("body: ", to_doc(body, opts))
    end)
  end
end
