defmodule Wialon.Packages.Login do
  defstruct ptype: "L",
            prot_version: nil,
            body: nil,
            status: nil,
            log: nil,
            respond: nil,
            finish: false

  def new([prot_version, imei, password | _] = msg, %{respond: respond}) do
    # TODO: crc проверка
    %__MODULE__{prot_version: prot_version, body: body(imei, password), status: status(msg)}
    |> Wialon.Packages.put_respond(respond)
    |> put_log()
    |> finalize()
  end

  def new([_imei, _password | _] = msg, %{respond: respond}), do: new(["1.1" | msg], %{respond: respond})

  def body(imei, password), do: %{imei: imei, password: password}

  def status([_prot_version, imei, password | _] = msg) do
    cond do
      invalid_crc?(msg) -> "10"
      invalid_password?(password) -> "01"
      invalid_imei?(imei) -> "0"
      true -> "1"
    end
  end

  # TODO: Проверка на то, что сумма совпадает
  defp invalid_crc?(_msg_list), do: false

  # TODO: Проверка на то, что imei должен быть зареган
  defp invalid_imei?(_imei), do: false

  # TODO: Проверка пароля
  defp invalid_password?(password) do
    case wialon_password() do
      list when is_list(list) -> password in list
      string when is_binary(string) -> password == string
      %Regex{} = regex -> Regex.match?(regex, password)
      _ -> throw("Необходимо добавить пароль в конфигурационный файл")
    end
    |> Kernel.!()
  end

  defp wialon_password, do: Application.get_env(:server, :wialon_password)

  defp put_log(mod),
    do: %__MODULE__{mod | log: %{log_type: "info", state: "authenticated", message: inspect(mod.body)}}

  defp finalize(%{status: "1"} = mod), do: mod
  defp finalize(mod), do: %__MODULE__{mod | finish: true}
end
