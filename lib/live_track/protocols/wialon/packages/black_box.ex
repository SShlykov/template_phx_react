defmodule Wialon.Packages.BlackBox do
  defstruct ptype: nil,
            body: nil,
            status: nil,
            log: nil,
            respond: nil

  def new(msg, %{respond: respond}) do
    # TODO: crc проверка
    package = body(msg)

    %__MODULE__{body: package, status: status(package)}
    |> Packages.put_respond(respond)
    |> put_log()
  end

  def body(msg), do: String.split(msg, "|")

  def status(body) do
    cond do
      invalid_crc?(body) -> ""
      true -> "#{length(body)}"
    end
  end

  # TODO: Проверка на то, что сумма совпадает
  defp invalid_crc?(_msg_list), do: false

  defp put_log(mod), do: %__MODULE__{mod | log: %{state: :black_box, message: inspect(mod)}}
end
