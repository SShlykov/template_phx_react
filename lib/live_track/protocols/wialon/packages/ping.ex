defmodule Wialon.Packages.Ping do
  defstruct ptype: nil,
            log: nil,
            respond: nil

  def new(_msg, %{respond: respond}) do
    # TODO: crc проверка
    %__MODULE__{}
    |> Packages.put_respond(respond)
    |> put_log()
  end

  defp put_log(mod), do: %__MODULE__{mod | log: %{state: :black_box, message: inspect(mod)}}

  def put_respond(mod, respond) do
    {resp, _} = Code.eval_string(respond, Map.to_list(mod))
    Map.put(mod, :respond, resp)
  rescue
    _e ->
      Map.put(mod, :respond, "#P#\r\n")
  end
end
