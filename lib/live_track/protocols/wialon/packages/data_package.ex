defmodule Wialon.Packages.DataPackage do
  import ShorterMaps
  defstruct ptype: nil,
            body: nil,
            respond: nil,
            status: nil,
            log: nil,
            finish: nil

  def new(msg, %{fields: fields, type: type, respond: respond}) do
    # TODO: crc проверка
    package = body(msg, fields)

    %__MODULE__{ptype: type, body: package, status: status(type, package, msg, fields)}
    |> Wialon.Packages.put_respond(respond)
    |> put_log()
    |> finalize()
  end

  def body(msg, fields) do
    Enum.zip(fields, msg)
    |> Enum.map(fn
      {:date, date} -> {:date, date}
      {:time, time} -> {:time, time}
      {:unzip_params, params} -> unzip_params(params)
      {k, "NA"} -> {k, nil}
      {k, v} -> {k, try_int(v)}
    end)
    |> List.flatten()
    |> Enum.into(%{})
    |> reparse()
    |> try_put_pack_dt
  end

  defp try_int(val) do
    {val, ""} = Integer.parse(val)
    val
  rescue
    _e -> :error
  end

  defp reparse(map) do
    require Logger
    ~M{lat1, lat2, lon1, lon2} = map

    Logger.warn(inspect(map))

    map
    |> Map.put(:dt, NaiveDateTime.local_now())
    |> Map.put(:lat, latify(lat1, lat2))
    |> Map.put(:lon, longify(lon1, lon2))
  end

  def latify(nil, _), do: nil
  def latify(lat, "S") do
    case latify(lat, "N") do
      :error -> :error
      lat -> -lat
    end
  end
  def latify(lat, "N") do
    int = String.to_float(lat) |> Kernel.*(:math.pow(10, 4)) |> ceil()
    grad = div(int, 1000000)
    min = rem(int, 1000000) / (600000)
    grad + Float.round(min, 6)
  rescue
    _e -> :error
  end
  def latify(_, _), do: :error

  def longify(nil, _), do: nil
  def longify(lon, "W") do
    case longify(lon, "E") do
      :error -> :error
      lon -> -lon
    end
  end
  def longify(lon, "E") do
    int = String.to_float(lon) |> Kernel.*(:math.pow(10, 4)) |> ceil()
    grad = div(int, 1000000)
    min = rem(int, 1000000) / (600000)
    grad + Float.round(min, 6)
  rescue
    _e -> :error
  end
  def longify(_, _), do: :error

  defp try_put_pack_dt(%{date: date, time: time} = pack) do
    {:ok, dt, _} = DateTime.from_iso8601("#{date}T#{time}Z")
    Map.put(pack, :pack_dt, dt)
  rescue
    _e ->
      Map.put(pack, :pack_dt, :error)
  end

  def unzip_params(params) do
    params
    |> String.split(",", trim: true)
    |> Enum.with_index()
    |> Enum.map(&unzip_param/1)
  rescue
    _e -> {:params, params}
  end

  def unzip_param({param, index}) do
    [string, type, value] = String.split(param, ":", trim: true)

    value =
      cond do
        type == "1" -> String.to_integer(value)
        type == "2" -> String.to_float(value)
        true -> value
      end

    {:"#{string}", value}
  rescue
    _e ->
      {:"error_#{index}", {:cant_unzip, param}}
  end

  def status("Short Data", package, msg, fields) do
    cond do
      length(msg) == length(fields) -> "-1"
      invalid_time?(package) -> "0"
      invalid_coordintes?(package) -> "10"
      invalid_speed?(package) -> "11"
      invalid_sattelites?(package) -> "12"
      # invalid_crc?(package) -> "13"
      true -> "1"
    end
  end

  def status("Extended Data", package, msg, fields) do
    cond do
      length(msg) == length(fields) -> "-1"
      invalid_time?(package) -> "0"
      invalid_coordintes?(package) -> "10"
      invalid_speed?(package) -> "11"
      invalid_sattelites?(package) -> "12"
      invalid_io?(package) -> "13"
      invalid_adc?(package) -> "14"
      invalid_additionals?(package) -> "15"
      invalid_crc?(package) -> "16"
      true -> "1"
    end
  end

  def invalid_structure?(_msg), do: false
  def invalid_time?(%{date: date, time: time}), do: !((!is_nil(date) && String.length(date) == 6) && (!is_nil(time) && String.length(time) == 6))
  def invalid_coordintes?(%{lat: lat, lon: lon}) do
    require Logger
    Logger.warn("lat: #{lat}, lon: #{lon}")
    (lat == :error || lon == :error || lat >= 90 || lat <= -90 || lon >= 180 || lon <= -180 )
  end
  def invalid_speed?(%{speed: speed}), do: speed == :error
  def invalid_sattelites?(%{sats: sats}), do: sats == :error
  def invalid_io?(_msg), do: false
  def invalid_adc?(_msg), do: false
  def invalid_additionals?(_msg), do: false
  def invalid_crc?(_msg), do: false

  defp put_log(mod), do: %__MODULE__{mod | log: %{log_type: "info", state: "server", message: inspect(mod.body)}}

  defp finalize(%{status: "1"} = mod), do: mod
  defp finalize(mod), do: %__MODULE__{mod | finish: true}
end
