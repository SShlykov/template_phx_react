defmodule LiveTrack.Logs do
  require Logger

  def do_log(%{log_type: type, protocol_name: protocol_name, state: "end", version: version}) do
    log_type = get_log_type(type)
    date_time = DateTime.utc_now()

    msg = """
    [#{date_time} #{log_type} #{protocol_name}::#{version}::end]
    -----------------------------------------------------------------
    """

    try_put_msg(msg)
  end

  def do_log(%{log_type: type, state: "end"}) do
    log_type = get_log_type(type)
    date_time = DateTime.utc_now()

    msg = """
    [#{date_time} #{log_type} end]
    -----------------------------------------------------------------
    """

    try_put_msg(msg)
  end

  def do_log(%{log_type: type, state: "connected", message: message}) do
    log_type = get_log_type(type)
    date_time = DateTime.utc_now()

    msg = """
    -----------------------------------------------------------------
    [#{date_time} #{log_type} connected]
    #{message}
    """

    try_put_msg(msg)
  end

  def do_log(%{
        log_type: type,
        protocol_name: protocol_name,
        state: state,
        version: version,
        message: message
      }) do
    log_type = get_log_type(type)
    date_time = DateTime.utc_now()

    msg = """
    [#{date_time} #{log_type} #{protocol_name}::#{version}::#{state}]
    #{message}
    """

    try_put_msg(msg)
  end

  def do_log(%{log_type: type, state: state, message: message}) do
    log_type = get_log_type(type)
    date_time = DateTime.utc_now()

    msg = """
    [#{date_time} #{log_type} #{state}]
    #{message}
    """

    try_put_msg(msg)
  end

  def try_put_msg(msg) do
    if Mix.env() !== :test do
      IO.puts(msg)
      write_log(msg)
    end
  rescue
    e ->
      Logger.error(e)
  end

  def write_log(msg) do
    dt = NaiveDateTime.local_now() |> Timex.format!("%Y-%m-%d", :strftime)
    path = "#{File.cwd!()}/priv/logs/#{dt}"
    file_name = "#{path}/logs.log"

    if !File.exists?(file_name) do
      File.mkdir_p(path)
      File.touch!(file_name)
    end

    File.write!(file_name, msg, [:append])
  end

  defp get_log_type("info"), do: "#{IO.ANSI.green()} INFO #{IO.ANSI.reset()}"
  defp get_log_type("warn"), do: "#{IO.ANSI.yellow()} WARNING #{IO.ANSI.reset()}"
  defp get_log_type("error"), do: "#{IO.ANSI.red()} ERROR #{IO.ANSI.reset()}"
end
