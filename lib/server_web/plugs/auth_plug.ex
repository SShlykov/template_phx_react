defmodule ServerWeb.Plugs.ValidateFrameBase do
  @moduledoc false
  @behaviour Plug

  import Plug.Conn
  import Phoenix.Controller

  def init(opts), do: opts

  def call(conn, _) do
    ["vniizht " <> usage_key] = get_req_header(conn, "authorization")
    %{"date" => date} = usage_key |> Base.decode64!() |> Jason.decode!()

    if abs(NaiveDateTime.diff(NaiveDateTime.from_iso8601!(date), NaiveDateTime.local_now())) <
         :timer.minutes(1) do
      conn
    else
      throw("WOW looks like boolshit")
    end
  rescue
    _e ->
      conn
      |> redirect(to: "/")
      |> halt()
  end
end
