defmodule ServerWeb.UserPlug do
  @moduledoc false
  import Plug.Conn, warn: false
  import Phoenix.Controller

  def init(default), do: default

  def call(
        %Plug.Conn{adapter: {_, adapter_params}, assigns: assigns, remote_ip: ip} = conn,
        _default
      ) do
    params = %{
      assigns: assigns,
      adapter_params: read_adapter_options(adapter_params),
      user_id: get_uid(assigns),
      browser: get_browser(adapter_params),
      remote_ip: stringify(ip)
    }

    conn
    |> save_session(params)
    |> save_device(params)
  end

  defp save_device(conn, %{user_id: user_id, browser: browser, remote_ip: remote_ip} = params) do
    case user_id &&
           Server.Niflheim.list_user_device_ips(
             user_id,
             Application.get_env(:live_monitor, :server_name, "live monitor")
           ) do
      [] ->
        {:ok, %{id: id}} =
          Server.Niflheim.create_user_device_ip(%{
            site: Application.get_env(:live_monitor, :server_name, "live monitor"),
            user_id: user_id,
            device_ip: remote_ip,
            device: browser,
            is_active: true,
            confirmed_dt: NaiveDateTime.local_now(),
            confirmed_by: "automatic"
          })

        conn
        |> put_session(:device, id)
        |> put_session(:check_device_info, true)

      list when is_list(list) ->
        validate_new_device(conn, params, known_devices: list)

      list when is_map(list) ->
        validate_new_device(conn, params, known_devices: [list])

      _ ->
        conn
    end
  end

  defp save_session(
         %Plug.Conn{
           cookies: cookies,
           halted: halted,
           method: method,
           request_path: path_info,
           resp_headers: resp_headers
         } = conn,
         %{
           remote_ip: remote_ip,
           assigns: assigns,
           adapter_params: adapter_params,
           user_id: user_id,
           browser: browser
         }
       ) do
    resp_headers = Enum.into(resp_headers, %{})

    info =
      %{
        cookies: cookies,
        halted: halted,
        method: method,
        assigns: assigns,
        resp_headers: resp_headers
      }
      |> Map.merge(adapter_params)
      |> Morphix.atomorphiform!()

    {:ok, %{id: session_id}} =
      Server.Niflheim.create_user_session(%{
        site: Application.get_env(:live_monitor, :server_name, "live monitor"),
        user_id: user_id,
        remote_ip: remote_ip,
        browser: browser,
        request_path: path_info,
        status: "ok",
        other: Poison.encode!(info),
        started_dt: NaiveDateTime.local_now(),
        comments: nil
      })

    put_session(conn, :session_id, session_id)
  rescue
    _e -> conn
  end

  defp validate_new_device(conn, %{user_id: user_id, browser: browser, remote_ip: remote_ip},
         known_devices: list
       ) do
    known_device = Enum.find(list, &(&1.device_ip == remote_ip))
    dev_len = length(list)

    cond do
      is_map(known_device) && known_device.confirmed_by &&
          Timex.diff(NaiveDateTime.local_now(), known_device.confirmed_dt, :second) > 0 ->
        Server.Niflheim.update_user_device_ip(known_device, %{is_active: true})

        if is_nil(known_device.device_type) do
          conn
          |> put_session(:device, known_device.id)
          |> put_session(:check_device_info, true)
        else
          conn
          |> put_session(:device, known_device.id)
          |> put_session(:check_device_info, false)
        end

      dev_len < 1000 && is_nil(known_device) ->
        {:ok, %{id: id}} =
          Server.Niflheim.create_user_device_ip(%{
            site: Application.get_env(:live_monitor, :server_name, "live monitor"),
            user_id: user_id,
            device_ip: remote_ip,
            device: browser,
            is_active: true,
            confirmed_dt: NaiveDateTime.local_now(),
            confirmed_by: "automatic"
          })

        conn
        |> put_session(:device, id)
        |> put_session(:check_device_info, true)

      is_nil(known_device) ->
        Server.Niflheim.create_user_device_ip(%{
          site: Application.get_env(:live_monitor, :server_name, "live monitor"),
          user_id: user_id,
          device_ip: remote_ip,
          device: browser,
          is_active: true,
          confirmed_dt: NaiveDateTime.local_now(),
          confirmed_by: "automatic"
        })

        conn
        |> put_flash(:info, "Максимальное количество устройств уже зарегистрировано")
        |> redirect(to: "/max_devices")
        |> halt()

      true ->
        conn
        |> put_flash(:info, "Устройство заблокировано или не предоставлен доступ")
        |> redirect(to: "/blocked_device")
        |> halt()
    end
  end

  defp get_uid(%{current_user: %{id: id}}) do
    Helpers.SqlSupQuery.run_query(
      "Update users SET logged_at = '#{NaiveDateTime.local_now()}' WHERE id = #{id}",
      :auth_db
    )

    id
  end

  defp get_uid(_), do: nil

  defp get_browser(%{headers: %{"user-agent" => user_agent}}), do: user_agent
  defp get_browser(_), do: nil

  defp read_adapter_options(adapter_params) do
    adapter_params
    |> Map.delete(:pid)
    |> Enum.map(fn
      {:peer, {host, pid}} -> {:peer, [stringify(host), pid]}
      {:sock, {host, pid}} -> {:sock, [stringify(host), pid]}
      other -> other
    end)
    |> Enum.into(%{})
  end

  defp stringify(tuple) when is_tuple(tuple) do
    Tuple.to_list(tuple) |> Enum.join(".")
  rescue
    _e -> nil
  end
end
