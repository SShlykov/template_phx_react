defmodule ServerWeb.Component.MenuItemComponent do
  use ServerWeb, :live_component

  def render(%{type: :simple} = assigns) do
    ~H"""
      <a {[class: ~s|my-1 rounded block p-2 text-xs cursor-pointer #{if(menu_item(@current_menu_item) == @menu_item.show, do: "bg-gray-300", else: "bg-white")} hover:bg-gray-200|, href: @menu_item.link]}>
        <%= @menu_item.show %>
      </a>
    """
  end

  def render(%{type: :group} = assigns) do
    ~H"""
    <li x-data="{ open: false }">
      <div @click="open = !open" {[class: ~s|my-1 items-center justify-between flex rounded block p-2 text-xs cursor-pointer #{if(menu_item(@current_menu_item) == @menu_item.show, do: "bg-gray-300", else: "bg-white")} hover:bg-gray-200|]}>
        <%= @menu_item.show %>
        <svg x-bind:class="open ? 'menu-triangl-active menu-triangl' : 'menu-triangl'" class="menu-triangl" fill="gray" viewBox="0 0 320 512"><path d="M143 352.3L7 216.3c-9.4-9.4-9.4-24.6 0-33.9l22.6-22.6c9.4-9.4 24.6-9.4 33.9 0l96.4 96.4 96.4-96.4c9.4-9.4 24.6-9.4 33.9 0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9l-136 136c-9.2 9.4-24.4 9.4-33.8 0z"/></svg>
       </div>
      <ul class="list-none" style="border-left: 2px solid #c0c0c0; padding-left: 5px;" x-show="open">
        <%= for {menu_sub_item, index} <- Enum.with_index(@menu_item.children) do %>
          <.live_component module={__MODULE__} id={"#{@id}_#{index}"} type={menu_sub_item.type} menu_item={menu_sub_item} current_menu_item={@current_menu_item} />
        <% end %>
      </ul>
    </li>
    """
  end

  def render(%{type: :line} = assigns) do
    ~H"""
      <hr class="m-4">
    """
  end

  def menu_item(%{show: show}), do: show
  def menu_item(show), do: show
end
