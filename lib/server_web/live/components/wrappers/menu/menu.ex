defmodule ServerWeb.Component.Menu do
  use ServerWeb, :live_component
  alias ServerWeb.Component.MenuItemComponent

  def render(%{current_menu_item: current_menu_item} = assigns)
      when is_binary(current_menu_item) do
    ~H"""
    <div class="border-r p-4 min-h-screen border-gray-200 bg-white fixed transition drop-shadow-md main-menu" x-bind:style=" showMenu ? 'left: 0' : 'left: -300px'">
      <div style="height: calc(100vh)" class="overflow-hidden overflow-y-auto">
        <ul class="m-0 pt-4">
          <%= for {menu_item, index} <- Enum.with_index(@menu_list) do %>
            <.live_component module={MenuItemComponent} id={"menu_item_component#{index}"} type={menu_item.type} menu_item={menu_item} current_menu_item={current_menu_item} />
          <% end %>
        </ul>
      </div>
    </div>
    """
  end
end
