defmodule ServerWeb.PageLive do
  @moduledoc false
  use ServerWeb, :live_view
  alias ServerWeb.Component.MenuContainer

  def mount(_, session, socket) do
    socket =
      socket
      |> assign_menu(session)
      |> assign(:current_menu_item, "Главная")

    {:ok, socket}
  end

  def render(assigns) do
    ~H"""
    <%= live_component MenuContainer, id: :main_menu_component, current_menu_item: @current_menu_item, current_user: @current_user, menu_list: @menu_list do %>
      <div  class="flex flex-col px-4"  style="width: 100vw">
        <%= live_react_component("Components.SearchPanel", [], id: "statistic_dashboards_page") %>
      </div>
    <% end %>
    """
  end

  def handle_params(_, _, socket) do
    socket =
      socket
      |> assign(:params, [])

    {:noreply, socket}
  end

  def handle_event("init_component", _search_panel_params, socket) do
    {:noreply, socket}
  end
end
