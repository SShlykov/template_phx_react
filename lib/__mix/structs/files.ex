defmodule Mix.File do
  defstruct path: nil,
            data: nil,
            context: nil,
            context_name: nil

  def new(path, context, context_name) do
    %__MODULE__{
      path: path,
      context: context,
      context_name: context_name,
      data: nil
    }
  end
end

defmodule Mix.Files do
  defstruct path: nil,
            file_path: nil,
            files: []

  def new(path, base_dir \\ "", context_name)

  def new(path, base_dir, context_name) when is_binary(path),
    do: new(%{path: path, files: ["index.jsx"]}, base_dir, context_name)

  def new(%{path: path, files: files}, base_dir, context_name) do
    folder_path = Path.join(base_dir, path)

    %__MODULE__{
      path: path,
      file_path: folder_path,
      files:
        Enum.map(files, fn file ->
          Mix.File.new("#{folder_path}/#{file}", path, context_name) |> Mix.ConstVals.init()
        end)
    }
  end
end
