defmodule Mix.Tasks.Push do
  @moduledoc """
  commit and pull module
  """
  use Mix.Task
  import HelperMix
  @avaliable_options ~w(-a -f -m -p)

  def run(opts) do
    [
      Enum.map(read_options(opts, @avaliable_options), &make_cmd/1),
      "git push origin master --quiet",
      "git push gitlab master --quiet"
    ]
    |> List.flatten()
    |> cmd

    IO.puts("Данные переданы в систему git.")
  end

  def make_cmd(["-p"]), do: Mix.Tasks.Pull.commands()
  def make_cmd(["-a"]), do: "git add ."
  def make_cmd(["-f" | file_paths]), do: ~s|git add #{Enum.join(file_paths, " ")}|
  def make_cmd(["-m", comment]), do: ~s|git commit -m "#{comment}" --quiet|
end
