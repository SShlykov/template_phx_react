defmodule Mix.Tasks.Gen.Page do
  @moduledoc """
  Pulls from all repositories
  """
  # import HelperMix
  use Mix.Task
  import HelperMix
  alias Mix.ReactContext

  @avaliable_options ~w(-n)

  def run(opts) do
    options = read_options(opts, @avaliable_options)

    with ["-n", page_name] <- Enum.find(options, :no_name, fn [hd | _] -> hd == "-n" end),
         %ReactContext{} = ctx <- ReactContext.new(page_name, ex_files: true),
         false <- ReactContext.pre_existing?(ctx) do
      Mix.Tasks.Gen.Cmp.run(opts)
      build_files(ctx)

      Mix.shell().info("""

      Страница создана: #{inspect(ctx.dir, pretty: true)}
      для использования в проекте добавьте в файл #{Atom.to_string(ctx.context_app)}_web/router.ex:
          live "/#{Phoenix.Naming.underscore(ctx.name)}", #{ctx.name}
      """)
    else
      :no_name -> info("Отсутствует имя контекста")
      _ -> info("Контекст уже существует")
    end
  end

  def info(text) do
    Mix.shell().info("""
    #{text}

    Задача gen.page ожидает передачу обязательного флага -n
    таким образом необходимо вызвать её по следующему принципу:

        mix gen.page -n MyPage

    Пожалуйста, убедитесь, что все параметры переданы правильно и
    данный контекст не существует
    """)
  end

  def to_create() do
    List.flatten([
      %{path: "tests", files: ["index.test.js"]},
      ~w(components styles funcs)
    ])
  end

  def build_files(context) do
    Mix.shell().info("""
    Приступаю к созданию файлов страницы #{context.name}
    """)

    make_files(context.base_file)
    :ok
  end

  defp make_directory(dir) do
    path = from_cwd(dir)
    File.mkdir_p!(path)
    Mix.shell().info("  Создана директория: #{dir}")
  end

  defp make_files(%Mix.ReactContext{files: files}) when is_list(files),
    do: Enum.map(files, &make_files/1)

  defp make_files(%Mix.Files{files: files, file_path: file_path}) do
    make_directory(file_path)
    Enum.each(files, &make_file/1)
  end

  defp make_file(%{path: path, data: data}) do
    dir = from_cwd(path)
    :ok = File.write!(dir, data)
    Mix.shell().info("    Создан документ: #{path}")
  end

  defp from_cwd(path), do: Path.join(File.cwd!(), path)
end
