import React from "react";
import { cond, T, equals, curry } from "ramda";

const up = (string) => {
  if (string) return string.charAt(0).toUpperCase() + string.slice(1);
  else {
    const error = `error by func capitalizeFirstLetter, ivalid string. string can't equal ${string}`;
    console.error(error);
    return error;
  }
};

const prepDefaultReducerActions = (defaultContext, contextData, action) => {
  return Object.entries(defaultContext).reduce(
    (acc, [key]) => [
      ...acc,
      [
        equals(`set${up(key)}`),
        () => ({
          ...contextData,
          [key]: action.payload,
        }),
      ],
      [
        equals(`c${up(key)}`),
        () => {
          const { param, value } = action.payload;
          return {
            ...contextData,
            [key]: {
              ...contextData[key],
              [param]: value,
            },
          };
        },
      ],
      [
        equals(`cByField${up(key)}`),
        () => {
          const { field, param, value } = action.payload;
          return {
            ...contextData,
            [key]: {
              ...contextData[key],
              [field]: {
                ...contextData[key][field],
                [param]: value,
              },
            },
          };
        },
      ],
    ],
    []
  );
};

/**
 * Описание полей бизнес логики компонента
 *
 * по дефолту создаются диспатчеры отностильно названия дефолтной переменной
 *
 * @example
 * const defaultComponentSettings = {};
 *
 * const defaultContextValue = {
 *    componentSettings: defaultComponentSettings,
 * };
 *
 * будет иметь диспатчеры:
 *
 * setComponentSettings -       примает на вход значение которое обновит componentSettings
 * cComponentSettings -         примает на вход объект вида { param, value }. Параметр (param) объекта componentSettings будет заменен на value
 * cByFieldComponentSettings -  примает на вход объект вида { field, param, value } - componentSettings[field][param] будет заменено на value
 */

const withContext = (
  ComponentContext,
  defaultContextValue,
  WrappedComponent
) => {
  const ModifiedComponent = (ownProps) => {
    const reducer = (contextData, action) => {
      return cond([
        ...prepDefaultReducerActions(defaultContextValue, contextData, action),

        [T, () => contextData],
      ])(action.type);
    };

    const [state, dispatch] = React.useReducer(reducer, defaultContextValue);

    const states = Object.entries(defaultContextValue).reduce(
      (acc, [key]) => ({
        ...acc,
        [`${key}State`]: [
          state[key],
          (value) =>
            dispatch({
              type: `set${up(key)}`,
              payload: value,
            }),
          curry((field, value) =>
            dispatch({
              type: `c${up(key)}`,
              payload: { param: field, value },
            })
          ),
          curry((mode, field, value) =>
            dispatch({
              type: `cByField${up(key)}`,
              payload: { field: mode, param: field, value },
            })
          ),
        ],
      }),
      {}
    );

    return (
      <ComponentContext.Provider value={{ states }}>
        <WrappedComponent {...ownProps} />
      </ComponentContext.Provider>
    );
  };

  return ModifiedComponent;
};


export { withContext };
