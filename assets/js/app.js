// import "react-dates/initialize";
import "phoenix_html";
import { Socket } from "phoenix";
import { LiveSocket } from "phoenix_live_view";
import {
  LiveReact,
  initLiveReact,
  loadUserData,
  FocusOnUpdate,
  ScrollOnUpdate,
} from "./helpers";
import topbar from "./topbar";
import morphdomCallbacks from "./morphdom_callbacks";
import { SearchPanel, KRSPS } from "./components/";

const hooks = {
  FocusOnUpdate,
  ScrollOnUpdate,
  LiveReact,
};

let csrfToken = document
  .querySelector("meta[name='csrf-token']")
  .getAttribute("content");
let liveSocket = new LiveSocket("/live", Socket, {
  params: () => {
    return {
      _csrf_token: csrfToken,
      user_data: loadUserData(),
    };
  },
  hooks: hooks,
  dom: morphdomCallbacks,
});

topbar.config({ barColors: { 0: "#29d" }, shadowColor: "rgba(0, 0, 0, .3)" });
window.addEventListener("phx:page-loading-start", (info) => topbar.show());
window.addEventListener("phx:page-loading-stop", (info) => topbar.hide());
document.addEventListener("DOMContentLoaded", (e) => {
  initLiveReact();
});

liveSocket.connect();

window.liveSocket = liveSocket;

window.Components = { SearchPanel, KRSPS };
