import { equals, drop } from "ramda";

const prepAction = (actionName, field, state, action) => [
  equals(actionName),
  () => {
    return {
      ...state,
      [field]: action.payload,
    };
  },
];

const prepReducer = ({ initialState, state, action }) =>
  Object.entries(initialState).map(([key]) =>
    prepAction(`CHANGE_${key}`, key, state, action)
  );

const prepEventes = (initialState, dispatch) =>
  Object.entries(initialState).reduce(
    (acc, [key]) => ({
      ...acc,
      [`c${key[0].toUpperCase()}${drop(1, key)}`]: (payload) =>
        dispatch({ type: `CHANGE_${key}`, payload }),
    }),
    {}
  );

export { prepReducer, prepEventes };
