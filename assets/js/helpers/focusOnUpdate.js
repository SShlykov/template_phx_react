import { isEditableElement } from "./checkers";

const FocusOnUpdate = {
  mounted() {
    this.__focus();
  },

  updated() {
    if (this.el !== document.activeElement) {
      this.__focus();
    }
  },

  __focus() {
    if (isEditableElement(document.activeElement)) {
      return;
    }

    this.el.focus();
    this.el.selectionStart = this.el.selectionEnd = this.el.value.length;
  },
};

export default FocusOnUpdate;
