/* eslint-disable import/no-anonymous-default-export */
import React, { lazy, Suspense } from "react";

import { initState } from "./HOCs";
import GraphicClient from "./components/GraphicClient";

const GraphicDownload = lazy(() => import("./components/GraphicDownload"));

const Graphic = (props) => (
  <>
    <GraphicClient {...props} />
    <div
      style={{
        position: "fixed",
        zIndex: -1,
      }}
    >
      <Suspense fallback={null}>
        <GraphicDownload {...props} />
      </Suspense>
    </div>
  </>
);

export default initState(Graphic);
