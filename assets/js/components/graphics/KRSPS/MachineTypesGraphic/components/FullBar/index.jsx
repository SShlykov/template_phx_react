import BarGraphic from "../BarGraphic";

export default ({ data = [], changeStep, setType, ...restProps }) => {
  return (
    <BarGraphic
      setData={setType}
      changeStep={changeStep}
      data={data.map(({ machines, classificationParentName, ...props }) => ({
        machine: `${classificationParentName || ""} (${machines})`,
        classificationParentName,
        ...props,
      }))}
      newStep={1}
      fieldName="classificationParent"
      {...restProps}
    />
  );
};
