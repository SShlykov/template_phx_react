import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  height: 880px;
  overflow-y: auto;
  overflow-x: hidden;
  position: relative;

  padding-top: 20px;
  justify-content: center;

  width: 100%;

  z-index: 1;
  * .apexcharts-menu-icon {
    opacity: 0;
  }

  * .apexcharts-title-text {
    opacity: 0 !important;
  }

  * .apexcharts-subtitle-text {
    opacity: 0 !important;
  }

  * .apexcharts-subtitle-text {
  }
`;

export const Container = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: center;
  width: 100%;
  height: 100%;
  padding: "0 20px 0 0";
  padding-top: 10px;
`;
