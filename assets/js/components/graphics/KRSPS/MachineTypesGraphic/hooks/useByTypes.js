import { useState } from "react";

export default ({ setData, dtStart, dtFinish, machinesidx }) => {
  const [loadingByTypes, setLoadingByTypes] = useState(false);

  const [error, setError] = useState(null);

  const getDataByTypes = () => {
    setLoadingByTypes(true);
    // request({
    //   schema: "ASKR_ON_OFF_REPORT_BYTYPES",
    //   token: localStorage.getItem("auth-token"),
    //   data: {
    //     start: dtStart,
    //     finish: dtFinish,
    //     filterCase: machinesidx,
    //   },
    // })
    //   .then((data) => {
    //     setLoadingByTypes(false);
    //     if (data?.getConnectionsReportDiagramData[0]) {
    //       setData(data?.getConnectionsReportDiagramData);
    //     }
    //   })
    //   .catch((e) => {
    //     console.log(e);
    //     retries < limit && setRetries(retries + 1);
    //     setError(e);
    //     setLoadingByTypes(false);
    //   });
  };

  return { getDataByTypes, loadingByTypes, error };
};
