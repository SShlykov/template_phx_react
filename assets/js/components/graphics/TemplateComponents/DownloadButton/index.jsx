import React, { useState } from "react";
import ButtonIconWrapper from "../ButtonIconWrapper";
import html2canvas from "html2canvas";
import { cond, T as TRUE } from "ramda";
import { Spin } from "antd";
import { randKey } from "../../../../helpers/frontend_utils";

const DownloadButton = ({
  containerId = "",
  isFullScreen = false,
  height = 410,
  width = 550,
}) => {
  const [downloading, setDownloading] = useState(false);

  if (isFullScreen) return null;

  const print = () => {
    setDownloading(true);
    const docWidth = document.body.offsetWidth;
    const diff = (1903 - document.body.offsetWidth) / 90;
    const diff2 = (1400 - document.body.offsetWidth) / 120;

    const x = cond([
      [() => docWidth > 1400, () => 18 - diff],
      [() => docWidth > 1000, () => 12 - diff2],
      [TRUE, () => 14 - diff2],
    ])(docWidth);

    const y = cond([
      [() => docWidth > 1400, () => 9],
      [TRUE, () => 16],
    ])(docWidth);

    const input = document.getElementById(containerId);

    html2canvas(input, {
      windowWidth: 1920,
      windowHeight: 1080,
      width,
      height,
    })
      .then((canvas) => {
        var a = document.createElement("a");
        a.href = canvas
          .toDataURL("image/jpeg")
          .replace("image/jpeg", "image/octet-stream");
        a.download = `${randKey()}.jpg`;
        a.click();
        setDownloading(false);
      })
      .catch((e) => {
        setDownloading(false);
        console.log(e);
      });
  };

  if (downloading) {
    return (
      <ButtonIconWrapper>
        <Spin
          size="small"
          style={{
            marginBottom: -5,
            opacity: 0.8,
          }}
        />
      </ButtonIconWrapper>
    );
  }

  return (
    <div className="download">
      <ButtonIconWrapper onClick={print} iconClassName="fal fa-download" />
    </div>
  );
};

export default DownloadButton;
