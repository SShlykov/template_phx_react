const MobileBreadButtons = ({ pathList, setStep }) =>
  pathList ? (
    pathList?.length && (
      <div
        style={{
          display: "flex",
          alignItems: "center",
          columnGap: 5,
        }}
      >
        {pathList
          ?.map(({ getData, name }, idx) => (
            <div
              style={{
                color: "gray",
                padding: 5,
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                border: "1px solid lightgray",
                fontSize: 10,
              }}
              onClick={() => {
                getData();
                setStep(idx);
              }}
            >
              {name}
            </div>
          ))
          .map((el, idx) => {
            if (pathList?.length === idx + 1) return el;

            return (
              <>
                {el}{" "}
                <span
                  style={{
                    color: "lightgray",
                  }}
                >
                  /
                </span>
              </>
            );
          })}
      </div>
    )
  ) : (
    <span></span>
  );

export default MobileBreadButtons;
