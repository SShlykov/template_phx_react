import React from "react";
import { cond, equals, T } from "ramda";
import { BreadBit, Wrapper } from "./styles";
import { Tooltip } from "antd";

const Description = ({
  dtStart,
  dtFinish,
  pathList = null,
  setStep = () => {},
}) => {
  const days =
    (new Date(dtFinish).getTime() - new Date(dtStart).getTime()) /
      1000 /
      60 /
      60 /
      24 +
    1;

  const dateText = cond([
    [equals(1), () => "технические сутки"],
    [equals(7), () => "За последнюю неделю"],
    [equals(30), () => "За последний месяц"],
    [T, () => null],
  ])(days);

  const text = `${dateText}`;

  return (
    <Tooltip
      title={days === 1 ? "С 18:00 предыдущего дня, до 18:00 текущего дня" : ""}
    >
      <Wrapper
        style={{
          marginBottom: 0,
        }}
      >
        {days === 1 && (
          <span
            style={{
              marginRight: 5,
              fontSize: 12.5,
            }}
          >
            За последние
          </span>
        )}
        <span
          className={days === 1 ? "__underline_dotted__" : ""}
          style={{
            fontSize: 12.5,
            cursor: "pointer",
          }}
        >
          {text}
        </span>
        {pathList && (
          <span
            style={{
              float: "left",
            }}
          >
            , выбрано:{" "}
          </span>
        )}
        {pathList && pathList?.length && (
          <span
            style={{
              display: "flex",
              columnGap: 5,
              paddingLeft: 5,
              float: "left",
            }}
          >
            {pathList?.map(({ getData, name }, idx) => (
              <BreadBit
                onClick={() => {
                  getData();
                  setStep(idx);
                }}
              >
                {name}
              </BreadBit>
            ))}
          </span>
        )}
      </Wrapper>
    </Tooltip>
  );
};

export const getDescription = ({ dtStart, dtFinish }) => {
  const days =
    (new Date(dtFinish).getTime() - new Date(dtStart).getTime()) /
      1000 /
      60 /
      60 /
      24 +
    1;

  return cond([
    [equals(1), () => "За последние технические сутки"],
    [equals(7), () => "За последнюю неделю"],
    [equals(30), () => "За последний месяц"],
    [T, () => null],
  ])(days);
};

export default Description;
