import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  border: 1px solid #e1e1e1;
  justify-content: center;
  border-radius: 3px;
  height: 30px;
  width: 30px;
  background-color: white;
  transition: all 0.3s;
  &:hover {
    border: 1px solid #a3a3a3;
  }
  @media screen and (max-width: 500px) {
    height: 30px;
    width: 30px;
    & * {
      transform: scale(0.9);
    }
  }
`;
