import React from "react";
import { Wrapper } from "./styles";

const ButtonIconWrapper = ({
  iconClassName = null,
  children = <></>,
  ...restProps
}) => {
  return (
    <Wrapper {...restProps}>
      {iconClassName && <i className={iconClassName} />}
      {children}
    </Wrapper>
  );
};

export default ButtonIconWrapper;
