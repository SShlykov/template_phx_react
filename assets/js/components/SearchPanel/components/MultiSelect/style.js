//@ts-nocheck
import styled from "styled-components";

export const Wrapper = styled.div`
  min-width: 170px;

  * .ant-select-selector {
    box-shadow: none !important;
    border: "1px solid rightgray" !important;
  }
`;
