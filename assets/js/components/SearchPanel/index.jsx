import React, { useState, useEffect } from "react";
import { initState } from "./HOC";
import { Select, Button } from "antd";
import { Wrapper, ContainerShadow, LeftPanel } from "./styles";
import { ViewSwitcher } from "../atoms";
import { parseSepareteDate } from "../../helpers/frontend_utils";
import { MultiSelect } from "./components";

const { Option } = Select;

const today = new Date() - 1000 * 60 * 60 * 24;
const lastWeek = new Date() - 1000 * 60 * 60 * 24 * 7;
const lastMonth = new Date() - 1000 * 60 * 60 * 24 * 30;

const SearchPanel = ({
  setDtStart,
  drpList,
  orgList,
  typeList,
  zavList,
  lStorage,
  cLStorage,
  setNeedUpdate,
  ...restProps
}) => {
  const [date, setDate] = useState(today);

  const setData = (date) => {
    setDtStart(parseSepareteDate(date, "-", "DateYMD"));
  };

  console.log(restProps);

  const push_search_params = (field, value) => {
    cLStorage(field, value);
    if (restProps.pushEvent) {
      restProps.pushEvent("update_search_params", [field, value]);
    }
  };

  return (
    <Wrapper>
      <LeftPanel>
        <MultiSelect
          className="my-select"
          list={drpList}
          field="drp_name"
          placeholder="Дорога"
          value={lStorage?.drp_name}
          onChange={(val) => {
            push_search_params("drp_name", val);
          }}
        />
        <MultiSelect
          className="my-select"
          list={orgList}
          field="org_name"
          value={lStorage?.org_name}
          placeholder="Предприятие"
          onChange={(val) => {
            push_search_params("org_name", val);
          }}
        />
        <MultiSelect
          className="my-select"
          list={typeList}
          field="type_name"
          value={lStorage?.type_name}
          placeholder="Тип машины"
          onChange={(val) => {
            push_search_params("type_name", val);
          }}
        />
        <MultiSelect
          className="my-select"
          list={zavList}
          value={lStorage?.zav_nomer}
          field="zav_nomer"
          placeholder="Заводской номер"
          onChange={(val) => {
            push_search_params("zav_nomer", val);
          }}
        />
        <Select
          value={date}
          onChange={(v) => {
            setData(v);
          }}
          className="select"
        >
          <Option title="За сутки" value={today}>
            За сутки
          </Option>
          <Option title="За неделю" value={lastWeek}>
            За неделю
          </Option>
          <Option title="За месяц" value={lastMonth}>
            За месяц
          </Option>
        </Select>
        <Button
          onClick={() => setNeedUpdate(true)}
          type="primary"
          className="button"
        >
          Выгрузить
        </Button>
      </LeftPanel>
      <ContainerShadow>
        <ViewSwitcher
          {...restProps}
          leftTitle="Общий вид"
          rightTitle="По типам"
        />
      </ContainerShadow>
    </Wrapper>
  );
};

export default initState(SearchPanel);
