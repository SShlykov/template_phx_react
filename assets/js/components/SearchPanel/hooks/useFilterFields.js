import { useEffect, useState, useLayoutEffect } from "react";
import { curry, cond, T, path, isEmpty, pipe, map, is, uniq } from "ramda";
import { parseSepareteDate, isArr } from "../../../helpers/frontend_utils";

const fullDrpList = [
  "Октябрьская",
  "Московская",
  "Горьковская",
  "Северная",
  "Северо-Кавказская",
  "Юго-Восточная",
  "Приволжская",
  "Куйбышевская",
  "Свердловская",
  "Южно-Уральская",
  "Западно-Сибирская",
  "Красноярская",
  "Восточно-Сибирская",
  "Забайкальская",
  "Дальневосточная",
];

const lastDay = new Date() - 1000 * 60 * 60 * 24;

const useFilterFields = ({ available_machines = [] }) => {
  const [c, cc] = useState(0); // каунтер для рендера страницы после обновления стора
  const [storage, setStorage] = useState(null);

  let lStorage = !storage ? {} : JSON.parse(storage.getItem("sRStorage")) || {};

  useLayoutEffect(() => {
    const drp_name = path(
      ["drp_name"],
      JSON.parse(localStorage.getItem("sRStorage"))
    );
    const org_name = path(
      ["org_name"],
      JSON.parse(localStorage.getItem("sRStorage"))
    );
    const type_name = path(
      ["type_name"],
      JSON.parse(localStorage.getItem("sRStorage"))
    );
    const zav_nomer = path(
      ["zav_nomer"],
      JSON.parse(localStorage.getItem("sRStorage"))
    );

    const needReset =
      !isArr(drp_name) ||
      !isArr(org_name) ||
      !isArr(type_name) ||
      !isArr(zav_nomer) ||
      !localStorage.getItem("sRStorage");
    setStorage(localStorage);
    if (needReset) {
      localStorage.setItem(
        "sRStorage",
        JSON.stringify({
          drp_name: [],
          org_name: [],
          type_name: [],
          zav_nomer: [],
        })
      );
      cc(c + 1);
    }
    cc(c + 1);
  }, []);

  const filterByField = curry((field, fieldName, list) => {
    let answer = [];
    try {
      answer =
        list &&
        list?.filter((el) =>
          cond([
            [isEmpty, () => el],
            [T, () => field.includes(el[fieldName])],
          ])(field)
        );
    } catch (error) {
      return answer;
    }
    return answer;
  });

  const drp_name = path(["drp_name"], lStorage) || [];
  const org_name = path(["org_name"], lStorage) || [];
  const type_name = path(["type_name"], lStorage) || [];
  const zav_nomer = path(["zav_nomer"], lStorage) || [];

  const filtredOrgList = filterByField(
    drp_name,
    "drp_name",
    available_machines
  );

  const machinesidx = pipe(
    filterByField(org_name, "org_name"),
    filterByField(type_name, "type_name"),
    filterByField(zav_nomer, "zav_nomer"),
    map((el) => el?.machineId)
  )(filtredOrgList);

  useEffect(() => {
    if (!isEmpty(available_machines)) {
      cond([
        [
          () => isEmpty(machinesidx) && !isEmpty(zav_nomer),
          () => {
            setTimeout(() => {
              cLStorage("zav_nomer")([]);
            }, 100);
          },
        ],
        [
          () => isEmpty(machinesidx) && !isEmpty(type_name),
          () => {
            setTimeout(() => {
              cLStorage("type_name")([]);
            }, 100);
          },
        ],
        [
          () => isEmpty(machinesidx) && !isEmpty(org_name),
          () => {
            setTimeout(() => {
              cLStorage("org_name")([]);
            }, 100);
          },
        ],
        [
          () => isEmpty(machinesidx) && !isEmpty(drp_name),
          () => {
            setTimeout(() => {
              cLStorage("drp_name")([]);
            }, 100);
          },
        ],
      ])();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [drp_name, org_name, type_name, zav_nomer]);

  const cLStorage = curry((name, val) => {
    localStorage.setItem(
      "sRStorage",
      JSON.stringify({
        ...lStorage,
        [name]: val,
      })
    );
    cc(c + 1);
  });

  const [dtStart, setDtStart] = useState(
    parseSepareteDate(lastDay, "-", "DateYMD")
  );

  const dtFinish = parseSepareteDate(lastDay, "-", "DateYMD");

  const reduceField = (field, list) =>
    list && uniq(list?.map((el) => el[field]));

  const filtredTypeList = pipe(
    () => filtredOrgList,
    filterByField(org_name, "org_name")
  )();
  const filtredZavList = pipe(
    () => filtredOrgList,
    filterByField(org_name, "org_name"),
    filterByField(type_name, "type_name")
  )();

  const userDrpList = reduceField("drp_name", available_machines);
  const drpList = fullDrpList.filter(
    (el) => is(Array, userDrpList) && userDrpList.includes(el)
  );

  const orgList = reduceField("org_name", filtredOrgList)
    .sort((a, b) => {
      const secondA = `${a}`.split("-")[1];
      const secondB = `${b}`.split("-")[1];
      return +secondA - +secondB;
    })
    .sort((a, b) => {
      const firstA = `${a}`.split("-")[0];
      const firstB = `${b}`.split("-")[0];
      return `${firstA}`.localeCompare(firstB);
    });

  const typeList = reduceField("type_name", filtredTypeList).sort();

  const zavList = reduceField("zav_nomer", filtredZavList).sort(
    (a, b) => +a - +b
  );

  return {
    dtStart,
    setDtStart,
    dtFinish,
    drp_name,
    org_name,
    type_name,
    zav_nomer,
    machinesidx,
    available_machines,
    drpList,
    orgList,
    typeList,
    zavList,
    cLStorage,
    lStorage,
  };
};

export default useFilterFields;
