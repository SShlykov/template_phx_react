import Config

db_host = "172.25.78.36"
app_port = 8080

config :server, Server.Repo,
  username: "postgres",
  password: "postgres",
  database: "live_track",
  hostname: db_host,
  show_sensitive_data_on_connection_error: true,
  pool_size: 2,
  log: false

config :server, ServerWeb.Endpoint,
  http: [ip: {0, 0, 0, 0}, port: 4000],
  check_origin: false,
  code_reloader: true,
  debug_errors: true,
  secret_key_base: "gZc/s/Wj6cOCmpEH1CBMKrFsQhsAMmrD68+Jy+ChHtSNAAfwmB2ijmhQIRBp279N",
  watchers: [
    esbuild:
      {Esbuild, :install_and_run,
       [
         :default,
         ~w(--sourcemap=inline --watch --loader:.jpg=file --loader:.png=file --loader:.woff=file --loader:.woff2=file --loader:.ttf=file --loader:.eot=file --loader:.gif=file --loader:.svg=file)
       ]},
    npx: [
      "tailwindcss",
      "--input=css/app.css",
      "--output=../priv/static/assets/app.css",
      "--postcss",
      "--watch",
      cd: Path.expand("../assets", __DIR__)
    ]
  ]

config :server, ServerWeb.Endpoint,
  live_reload: [
    patterns: [
      ~r"priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$",
      ~r"priv/gettext/.*(po)$",
      ~r"lib/cdrp_web/(live|views)/.*(ex)$",
      ~r"lib/cdrp_web/templates/.*(eex)$"
    ]
  ]

config :logger, :console, format: "[$level] $message\n"
config :phoenix, :stacktrace_depth, 20
config :phoenix, :plug_init_mode, :runtime
