defmodule Test.WialonTest do
  use ExUnit.Case
  import Mock, warn: false

  @tcp_attrs [:binary, packet: :line, active: false, reuseaddr: true]

  setup do
    {:ok, _} = Application.ensure_all_started(:server)
    Node.start(:"test_tcp@172.25.78.153")
    Node.set_cookie(:"8FWrDX6nB9zCreE5")
    Node.connect(:"global@172.25.78.153")
    :ok
  end

  describe "wialon v1.1 // [пакет авторизации]:" do
    setup do
      port = Application.get_env(:server, :port) || 9876
      host = "172.25.78.153" |> String.to_charlist()
      # Тесты работают паралельно, поэтому к imei нужно добавлять число с номером теста
      imei = 123_456_789

      %{port: port, host: host, auth: %{imei: imei, password: "valid_password"}}
    end

    test "валидный imei и пароль", %{auth: %{imei: imei, password: password}} = sock_info do
      socket = connect(sock_info)
      {:ok, reply} = send_and_fetch(socket, "#L##{imei}1;#{password}\r\n")
      :gen_tcp.close(socket)
      assert reply == "#AL#1\r\n"
    end

    test "невалидный пароль", %{auth: %{imei: imei}} = sock_info do
      socket = connect(sock_info)
      {:ok, reply} = send_and_fetch(socket, "#L##{imei}2;invalid_passwd\r\n")
      :gen_tcp.close(socket)
      assert reply == "#AL#01\r\n"
    end
  end

  describe "wialon v1.1 // [сокращенный пакет]:" do
    setup do
      port = Application.get_env(:server, :port) || 9876
      host = "172.25.78.153" |> String.to_charlist()
      # Тесты работают паралельно, поэтому к imei нужно добавлять число с номером теста
      imei = 123_456_789

      %{port: port, host: host, auth: %{imei: imei, password: "valid_password"}}
    end

    test "валидный пакет", %{auth: %{imei: imei, password: password}} = sock_info do
      socket = connect(sock_info)
      {:ok, _reply} = send_and_fetch(socket, "#L##{imei}3;#{password}\r\n")
      {:ok, reply} = send_and_fetch(socket, "#SD##{curr_date()};#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;13\r\n")
      :gen_tcp.close(socket)
      assert reply == "#ASD#1\r\n"
    end

    test "не валидная дата/время", %{auth: %{imei: imei, password: password}} = sock_info do
      socket = connect(sock_info)
      {:ok, _reply} = send_and_fetch(socket, "#L##{imei}4;#{password}\r\n")
      {:ok, reply} = send_and_fetch(socket, "#SD#0;#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;13\r\n")
      :gen_tcp.close(socket)
      assert reply == "#ASD#0\r\n"
    end

    test "не валидные координаты", %{auth: %{imei: imei, password: password}} = sock_info do
      socket = connect(sock_info)
      {:ok, _reply} = send_and_fetch(socket, "#L##{imei}5;#{password}\r\n")
      {:ok, reply} = send_and_fetch(socket, "#SD#0;#{curr_time()};55044.6025;N;03739.6834;E;10;56;100;13\r\n")
      :gen_tcp.close(socket)
      assert reply == "#ASD#0\r\n"
    end
  end

  defp curr_date() do
    {:ok, date} = Timex.format(:calendar.local_time(), "%d%m%y", :strftime)
    date
  end

  defp curr_time() do
    {:ok, time} = Timex.format(:calendar.local_time(), "%H%M%S", :strftime)
    time
  end

  defp connect(%{host: host, port: port}) do
    {:ok, socket} = :gen_tcp.connect(host, port, @tcp_attrs)
    socket
  end

  defp send_and_fetch(socket, msg, timeout \\ 1000) do
    :ok = send_msg(socket, msg)
    await_msg(socket, timeout)
  end

  defp send_msg(socket, msg), do: :gen_tcp.send(socket, msg)
  defp await_msg(socket, timeout), do: :gen_tcp.recv(socket, 0, timeout)
end
