defmodule Server.MixProject do
  use Mix.Project

  def project do
    [
      app: :server,
      version: "0.1.0",
      elixir: "~> 1.12",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      releases: releases()
    ]
  end

  defp releases do
    [
      server: [
        include_erts: true,
        path: "site_template",
        cookie: "cookie_template",
        include_executables_for: [:unix],
        applications: [
          runtime_tools: :permanent,
          server: :permanent
        ]
      ]
    ]
  end

  def application do
    [
      mod: {Server.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp deps do
    [
      {:phoenix, "~> 1.6.6"},
      {:phoenix_ecto, "~> 4.4"},
      {:phoenix_view, "~> 1.0"},
      {:ecto_sql, "~> 3.6"},
      {:scrivener_ecto, "~> 2.0"},
      {:postgrex, ">= 0.0.0"},
      {:phoenix_html, "~> 3.0"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:phoenix_live_view, "~> 0.17.5"},
      {:floki, ">= 0.30.0", only: :test},
      {:phoenix_live_dashboard, "~> 0.6"},
      {:esbuild, "~> 0.3", runtime: Mix.env() == :dev},
      {:swoosh, "~> 1.3"},
      {:poison, "~> 4.0"},
      {:telemetry_metrics, "~> 0.6"},
      {:telemetry_poller, "~> 1.0"},
      {:gettext, "~> 0.18"},
      {:jason, "~> 1.2"},
      {:plug_cowboy, "~> 2.5"},
      {:pbkdf2_elixir, "~> 1.0"},
      {:dataloader, "~> 1.0"},
      {:comeonin, "~> 5.1"},
      {:bcrypt_elixir, "~> 2.0"},
      {:guardian, "~> 2.2.1"},
      {:plug, "~> 1.0"},
      {:corsica, "~> 1.0"},
      {:morphix, "~> 0.8.0"},
      {:csv, "~> 2.3"},
      {:observer_cli, "~> 1.5"},
      {:timex, "~> 3.5"},
      {:elixlsx, "~> 0.4.2"},
      {:tesla, "~> 1.4.3"},
      {:hackney, "~> 1.17.0"},
      {:recase, "~> 0.5"},
      {:shorter_maps, "~> 2.0"},
      {:geo, "~> 3.4.2"},
      {:geo_postgis, "~> 3.4"},
      {:castore, "~> 0.1.0"},
      {:vega_lite, "~> 0.1.0"},
      {:kino, "~> 0.2.0"},
      {:libcluster, "~> 3.2"},
      {:munin, git: "http://172.25.78.108/scandinavians/munin.git", tag: "v0.1.11"},
      {:r_filer, git: "http://172.25.78.108/scandinavians/r_filer.git", tag: "v0.1.3"},
      {:cachex, "~> 3.4"},
      {:geocalc, "~> 0.8"},
      {:gen_smtp, "~> 1.1.1"},
      {:mock, "~> 0.3.7", only: :test}
    ]
  end

  defp aliases do
    [
      setup: ["deps.get", "ecto.setup"],
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate --quiet", "test"],
      "assets.deploy": ["esbuild default --minify", "phx.digest"],
      "dev.setup": ["deps.get", "cmd npm install --prefix assets"],
      "dev.build": ["cmd npm run deploy --prefix ./assets"],
      "format.all": ["format", "cmd npm run format --prefix ./assets"],
      "escript.build": ["loadconfig", "escript.build"]
    ]
  end
end
